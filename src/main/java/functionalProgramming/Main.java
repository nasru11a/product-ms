package functionalProgramming;

import java.util.function.Predicate;

public class Main {

    public static void print(Account account, Predicate<Account> checker){
        if (checker.test(account)) {
            System.out.println(account);
        }
    }

    public static void main(String[] args) {
        print(new Account(100.0, "USD", false), account -> account.isExpired);
        print(new Account(92.0, "USD", true), account -> account.balance > 90);
    }
}
