package functionalProgramming;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Account {
    Double balance;
    String currencyType;
    boolean isExpired;
}
