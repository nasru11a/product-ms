package az.ingress.product.repository;

import az.ingress.product.model.Manufacturer;
import az.ingress.product.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ManufacturerRepository extends JpaRepository<Manufacturer, Long> {

}
