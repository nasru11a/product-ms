package az.ingress.product.dto;

import az.ingress.product.model.Category;
import az.ingress.product.model.Manufacturer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductDto {

    private String name;
    private BigDecimal price;
    private Set<CategoryDto> categoryDto;
    private Integer count;
    private ManufacturerDto manufacturerDto;
}
