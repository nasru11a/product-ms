package az.ingress.product.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConfigMapper {

    @Bean
    public org.modelmapper.ModelMapper getModelMapper(){
        return new org.modelmapper.ModelMapper();
    }

}
