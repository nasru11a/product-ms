package az.ingress.product.service;

import az.ingress.product.config.NotFoundException;
import az.ingress.product.dto.CategoryDto;
import az.ingress.product.dto.ManufacturerDto;
import az.ingress.product.dto.ProductDto;
import az.ingress.product.exception.ProductNotFoundException;
import az.ingress.product.model.Category;
import az.ingress.product.model.Manufacturer;
import az.ingress.product.model.Product;
import az.ingress.product.repository.ManufacturerRepository;
import az.ingress.product.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService{

    private final ProductRepository productRepository;
    private final ModelMapper mapper;

    @Override
    public ProductDto createProduct(ProductDto productDto) {
        log.info("Creating a product.");
        Manufacturer manufacturer = mapper.map(productDto.getManufacturerDto(), Manufacturer.class);
        Set<Category> categories = productDto.getCategoryDto()
                .stream().map((categoryDto -> mapper.map(categoryDto, Category.class)))
                .collect(Collectors.toSet());

        Product product = mapper.map(productDto,Product.class);
        product.setManufacturer(manufacturer);
        product.setCategories(categories);

        ProductDto savedDto = mapper.map(productRepository.save(product), ProductDto.class);

        ManufacturerDto savedManufacturer = mapper.map(manufacturer, ManufacturerDto.class);
        Set<CategoryDto> savedCategoryDto = categories
                .stream().map((category -> mapper.map(category, CategoryDto.class)))
                .collect(Collectors.toSet());

        savedDto.setManufacturerDto(savedManufacturer);
        savedDto.setCategoryDto(savedCategoryDto);

        return savedDto;
    }

    @Override
    public ProductDto getProductById(Long id) {
        return productRepository
                .findById(id)
                .map(product -> {
                    ProductDto productDto = mapper.map(product, ProductDto.class);
                    return productDto;
                })
                .orElseThrow(() -> new ProductNotFoundException(id));
    }

    @Override
    public ProductDto updateProduct(Long id, ProductDto productDto) {
        return productRepository
                .findById(id)
                .map(product -> {
                    BeanUtils.copyProperties(productDto, product);
                    Product saved = productRepository.save(product);
                    return mapper.map(saved, ProductDto.class);
                })
                .orElseThrow(() -> new ProductNotFoundException(id));
    }

    @Override
    public void deleteProductById(Long id) {
        try{
            productRepository.deleteById(id);
        }
        catch (EmptyResultDataAccessException ex) {
            throw new NotFoundException(String.format(ProductNotFoundException.getMESSAGE(), id), ex);
        }
    }
}
