package az.ingress.product.service;

import az.ingress.product.dto.ProductDto;
import org.springframework.stereotype.Service;

@Service
public interface ProductService {

    ProductDto createProduct(ProductDto productDto);
    ProductDto getProductById(Long id);
    ProductDto updateProduct(Long id, ProductDto productDto);
    void deleteProductById(Long id);
}

