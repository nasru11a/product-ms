package az.ingress.product.controller;

import az.ingress.product.dto.ProductDto;
import az.ingress.product.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/product")
public class ProductController {

    private final ProductService service;

    @PostMapping
    public ProductDto createProduct(@RequestBody ProductDto productDto){
        return service.createProduct(productDto);
    }

    @GetMapping("/{id}")
    public ProductDto getProductById(@PathVariable Long id){
        return service.getProductById(id);
    }
}
