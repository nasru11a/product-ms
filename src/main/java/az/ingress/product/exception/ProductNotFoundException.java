package az.ingress.product.exception;

import az.ingress.product.config.NotFoundException;
import lombok.Getter;

public class ProductNotFoundException extends NotFoundException {

    private static final long serialVersionUID = 58432132487812L;

    @Getter
    private static final String MESSAGE = "No product with id %s was found in the system.";

    public ProductNotFoundException(Long id) {
        super(String.format(MESSAGE, id));
    }

}
