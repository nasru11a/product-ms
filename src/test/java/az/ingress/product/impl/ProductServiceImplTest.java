package az.ingress.product.impl;

import az.ingress.product.config.NotFoundException;
import az.ingress.product.dto.ProductDto;
import az.ingress.product.exception.ProductNotFoundException;
import az.ingress.product.model.Product;
import az.ingress.product.repository.ProductRepository;
import az.ingress.product.service.ProductServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.dao.EmptyResultDataAccessException;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ProductServiceImplTest {

    private static final Long ID = 1001L;
    private static final String NAME = "IPhone";
    private static final BigDecimal PRICE = BigDecimal.valueOf(1000);
    private static final LocalDate CREATION_DATE = LocalDate.of(2022,1,1);
    private static final String BRAND = "Apple";
    private static final Integer COUNT = 100;
    private static final Boolean IS_ACTIVE = true;

    @InjectMocks
    private ProductServiceImpl service;

    @Mock
    private ProductRepository repository;

    @Spy
    private ModelMapper mapper;

    @Test
    void givenProductDtoWhenCreateThenSave(){
        //Arrange
        final ProductDto requestDto = returnRequestDto();
        final Product product = mapper.map(requestDto, Product.class);
        when(repository.save(any(Product.class))).thenReturn(product);

        //Act
        ProductDto responseDto = service.createProduct(requestDto);

        //Assert
        verify(repository, times(1)).save(product);

        assertThat(responseDto.getName()).isEqualTo(product.getName());
        assertThat(responseDto.getPrice()).isEqualTo(product.getPrice());
        assertThat(responseDto.getCreationDate()).isEqualTo(product.getCreationDate());
        assertThat(responseDto.getBrand()).isEqualTo(product.getBrand());
        assertThat(responseDto.getCount()).isEqualTo(product.getCount());
        assertThat(responseDto.getIsActive()).isEqualTo(product.getIsActive());
    }

    @Test
    void givenProductIdWhenGetThenProductDto(){
        //Arrange
        Product product = returnProduct();
        when(repository.findById(any(Long.class))).thenReturn(Optional.of(product));

        //Act
        ProductDto response = service.getProductById(ID);

        //Assert
        verify(repository, times(1)).findById(ID);
        assertThat(response.getId()).isEqualTo(ID);
    }

    @Test
    void givenProductIdDoesNotMatchProductWhenGetThenException(){
        //Arrange
        String message = ProductNotFoundException.getMESSAGE();
        when(repository.findById(any(Long.class))).thenReturn(Optional.empty());

        //Assert
        assertThatThrownBy(() -> service.getProductById(ID))
                .isInstanceOf(NotFoundException.class)
                .hasMessage(message);
    }

    @Test
    void givenInputIsValidWhenUpdateThenSave(){
        //Arrange
        ProductDto requestDto = returnRequestDto();
        Product product = mapper.map(requestDto, Product.class);
        when(repository.findById(any(Long.class))).thenReturn(Optional.of(product));
        when(repository.save(any(Product.class))).thenReturn(product);

        //Act
        ProductDto responseDto = service.updateProduct(ID, requestDto);

        //Assert
        assertThat(responseDto.getName()).isEqualTo(product.getName());
        assertThat(responseDto.getPrice()).isEqualTo(product.getPrice());
        assertThat(responseDto.getCreationDate()).isEqualTo(product.getCreationDate());
        assertThat(responseDto.getBrand()).isEqualTo(product.getBrand());
        assertThat(responseDto.getCount()).isEqualTo(product.getCount());
        assertThat(responseDto.getIsActive()).isEqualTo(product.getIsActive());
    }

    @Test
    void givenIdDoesNotMatchAnyProductWhenUpdateThenException(){
        //Arrange
        ProductDto requestDto = returnRequestDto();
        String message = ProductNotFoundException.getMESSAGE();
        when(repository.findById(any(Long.class))).thenReturn(Optional.empty());

        //Assert
        assertThatThrownBy(() -> service.updateProduct(ID,requestDto))
                .isInstanceOf(ProductNotFoundException.class)
                .hasMessage(message);
    }

    @Test
    void givenIdWhenDeleteByIdThenDelete(){
        //Act
        service.deleteProductById(ID);

        //Assert
        verify(repository, times(1)).deleteById(ID);
    }

    @Test
    void givenIdDoesNotMatchAnyProductWhenDeleteThenException(){
        //Arrange
        String message = String.format(ProductNotFoundException.getMESSAGE(), ID);
        doThrow(EmptyResultDataAccessException.class).when(repository).deleteById(ID);

        //Act & Assert
        assertThatThrownBy(() -> service.deleteProductById(ID))
                .isInstanceOf(NotFoundException.class)
                .hasMessage(message);
    }

    private ProductDto returnRequestDto(){
        return ProductDto
                .builder()
                .id(ID)
                .name(NAME)
                .price(PRICE)
                .creationDate(CREATION_DATE)
                .brand(BRAND)
                .count(COUNT)
                .isActive(IS_ACTIVE)
                .build();
    }

    private Product returnProduct(){
        return Product
                .builder()
                .id(ID)
                .name(NAME)
                .price(PRICE)
                .creationDate(CREATION_DATE)
                .brand(BRAND)
                .count(COUNT)
                .isActive(IS_ACTIVE)
                .build();
    }
}
